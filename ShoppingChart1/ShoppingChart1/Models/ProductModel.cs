﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingChart1.Models
{
    public class ProductModel
    {
        private List<Product> products;
        public ProductModel()
        {
            this.products = new List<Product>()
            {
                new Product()
                {
                    Id="01",
                    Name="Cipele na stiklu",
                    Price=1000,
                    Photo="cipele-na-stiklu-5010-crvene_11290.jpg"

                },
                new Product()
                {
                    Id="02",
                    Name="Elegantne muske cipele",
                    Price=1500,
                    Photo="elegantne-muske-cipele-2062-29f-braon_486968.jpg"
                },
                new Product()
                {
                    Id="03",
                    Name=" Muski sorc",
                    Price=2000,
                    Photo="Muski sorc.jpg"
                },
                new Product()
                {
                    Id="04",
                    Name="Patike muske bele",
                    Price=1800,
                    Photo="Patike muske bele.jpg"
                },
                new Product()
                {
                    Id="05",
                    Name="Zenska torba i haljina",
                    Price=2500,
                    Photo="Zenska torba i haljina.jpg"
                },
                new Product()
                {
                    Id="06",
                    Name="Zenske helanke",
                    Price=2800,
                    Photo="Zenske helanke.jpg"
                },
                new Product()
                {
                    Id="07",
                    Name="Cipele na stiklu crne",
                    Price=3000,
                    Photo="cipele-na-stiklu-5010-crne_11272.jpg"
                },
                 new Product()
                {
                    Id="08",
                    Name="Decija zenska jakna",
                    Price=2500,
                    Photo="Decija zenska jakna.jpg"
                },
                  new Product()
                {
                    Id="09",
                    Name="Muske patike braon",
                    Price=2500,
                    Photo="Muske patike braon.jpg"
                },
                   new Product()
                {
                    Id="10",
                    Name="Plavi duks",
                    Price=1800,
                    Photo="Plavi duks.jpg"
                }

            };
        }
        public List<Product> findAll()
        {
            return this.products;
        }
        public Product find(string id)
        {
            return this.products.Single(products => products.Id.Equals(id));
        }
    }
}