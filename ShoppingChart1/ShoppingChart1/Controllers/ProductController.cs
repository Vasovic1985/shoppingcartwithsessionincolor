﻿using ShoppingChart1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ShoppingChart1.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(int? page)
        {
            ProductModel productModel = new ProductModel();

            ViewBag.products = productModel.findAll();
            
            return View();
        }
    }
}